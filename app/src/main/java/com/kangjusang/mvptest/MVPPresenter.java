package com.kangjusang.mvptest;

public interface MVPPresenter {
    void addNum(int a, int b);
    void saveData(int data);
}
