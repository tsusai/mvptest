package com.kangjusang.mvptest;

public interface MVPView {
    void showResult(int result);
}
