package com.kangjusang.mvptest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import static com.kangjusang.mvptest.R.id.tvResult01;

public class MainActivity extends AppCompatActivity implements MVPView {

    MVPPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPresenter = new MainPresenter(this);

        initListener();
    }

    private void initListener() {
        final TextInputLayout inputLayoutA = findViewById(R.id.textInputLayoutA);
        final TextInputLayout inputLayoutB = findViewById(R.id.textInputLayoutB);
        Button btnEqual = findViewById(R.id.btnEqual);

        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputA = inputLayoutA.getEditText().getText().toString();
                String inputB = inputLayoutB.getEditText().getText().toString();

                int intA = Integer.valueOf(inputA);
                int intB = Integer.valueOf(inputB);

                mainPresenter.addNum(intA, intB);
            }
        });
    }

    @Override
    public void showResult(int result) {
        TextView tvResult = (TextView)findViewById(tvResult01);
        tvResult.setText(Integer.toString(result));
    }
}
