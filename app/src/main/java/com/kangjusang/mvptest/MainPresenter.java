package com.kangjusang.mvptest;

public class MainPresenter implements MVPPresenter {

    MVPView mainView;
    MainModel mainModel;

    public MainPresenter(MVPView view) {
        mainView = view;
        mainModel = new MainModel(this);
    }

    @Override
    public void addNum(int a, int b) {
        mainView.showResult(a + b);
    }

    @Override
    public void saveData(int data) {
        mainModel.saveData(data);
    }
}
